<?php

/**
 * @file
 * uw_ct_feds_hp_hero.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_hp_hero_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'override all promote to front page option'.
  $permissions['override all promote to front page option'] = array(
    'name' => 'override all promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_hero_image promote to front page option'.
  $permissions['override feds_homepage_hero_image promote to front page option'] = array(
    'name' => 'override feds_homepage_hero_image promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
