<?php

/**
 * @file
 * uw_ct_feds_hp_hero.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_hp_hero_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_homepage_hero_split';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds Homepage Hero Split';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Feds Homepage Hero Split';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'hero-split-container';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Background Colour Left */
  $handler->display->display_options['fields']['field_background_colour_left']['id'] = 'field_background_colour_left';
  $handler->display->display_options['fields']['field_background_colour_left']['table'] = 'field_data_field_background_colour_left';
  $handler->display->display_options['fields']['field_background_colour_left']['field'] = 'field_background_colour_left';
  $handler->display->display_options['fields']['field_background_colour_left']['label'] = '';
  $handler->display->display_options['fields']['field_background_colour_left']['element_type'] = '0';
  $handler->display->display_options['fields']['field_background_colour_left']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_background_colour_left']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_background_colour_left']['element_wrapper_class'] = 'hero-bg-color-left';
  /* Field: Content: Hero Image Left */
  $handler->display->display_options['fields']['field_main_hero_image']['id'] = 'field_main_hero_image';
  $handler->display->display_options['fields']['field_main_hero_image']['table'] = 'field_data_field_main_hero_image';
  $handler->display->display_options['fields']['field_main_hero_image']['field'] = 'field_main_hero_image';
  $handler->display->display_options['fields']['field_main_hero_image']['label'] = '';
  $handler->display->display_options['fields']['field_main_hero_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_main_hero_image']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_main_hero_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_main_hero_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Background Colour Right */
  $handler->display->display_options['fields']['field_background_colour_right']['id'] = 'field_background_colour_right';
  $handler->display->display_options['fields']['field_background_colour_right']['table'] = 'field_data_field_background_colour_right';
  $handler->display->display_options['fields']['field_background_colour_right']['field'] = 'field_background_colour_right';
  $handler->display->display_options['fields']['field_background_colour_right']['label'] = '';
  $handler->display->display_options['fields']['field_background_colour_right']['element_type'] = '0';
  $handler->display->display_options['fields']['field_background_colour_right']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_background_colour_right']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_background_colour_right']['element_wrapper_class'] = 'hero-bg-color-right';
  /* Field: Content: Hero Image Right */
  $handler->display->display_options['fields']['field_hero_image_right']['id'] = 'field_hero_image_right';
  $handler->display->display_options['fields']['field_hero_image_right']['table'] = 'field_data_field_hero_image_right';
  $handler->display->display_options['fields']['field_hero_image_right']['field'] = 'field_hero_image_right';
  $handler->display->display_options['fields']['field_hero_image_right']['label'] = '';
  $handler->display->display_options['fields']['field_hero_image_right']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_hero_image_right']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_hero_image_right']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_hero_image_right']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_hero_image_right']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Primary Image */
  $handler->display->display_options['fields']['field_primary_image']['id'] = 'field_primary_image';
  $handler->display->display_options['fields']['field_primary_image']['table'] = 'field_data_field_primary_image';
  $handler->display->display_options['fields']['field_primary_image']['field'] = 'field_primary_image';
  $handler->display->display_options['fields']['field_primary_image']['label'] = '';
  $handler->display->display_options['fields']['field_primary_image']['element_class'] = 'no-display';
  $handler->display->display_options['fields']['field_primary_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_primary_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_primary_image']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_primary_image']['type'] = 'list_key';
  $handler->display->display_options['fields']['field_primary_image']['settings'] = array(
    'data_element_key' => '',
    'skip_safe' => 0,
    'skip_empty_values' => 0,
  );
  /* Field: Content: Subtitle */
  $handler->display->display_options['fields']['field_hero_subtitle']['id'] = 'field_hero_subtitle';
  $handler->display->display_options['fields']['field_hero_subtitle']['table'] = 'field_data_field_hero_subtitle';
  $handler->display->display_options['fields']['field_hero_subtitle']['field'] = 'field_hero_subtitle';
  $handler->display->display_options['fields']['field_hero_subtitle']['label'] = '';
  $handler->display->display_options['fields']['field_hero_subtitle']['element_type'] = '0';
  $handler->display->display_options['fields']['field_hero_subtitle']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_hero_subtitle']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_hero_subtitle']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_hero_subtitle']['element_default_classes'] = FALSE;
  /* Field: Content: Flexible Heading */
  $handler->display->display_options['fields']['field_flexible_heading']['id'] = 'field_flexible_heading';
  $handler->display->display_options['fields']['field_flexible_heading']['table'] = 'field_data_field_flexible_heading';
  $handler->display->display_options['fields']['field_flexible_heading']['field'] = 'field_flexible_heading';
  $handler->display->display_options['fields']['field_flexible_heading']['label'] = '';
  $handler->display->display_options['fields']['field_flexible_heading']['element_type'] = '0';
  $handler->display->display_options['fields']['field_flexible_heading']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_flexible_heading']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_flexible_heading']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_flexible_heading']['element_default_classes'] = FALSE;
  /* Field: Content: Hero Body Content */
  $handler->display->display_options['fields']['field_hero_body_content']['id'] = 'field_hero_body_content';
  $handler->display->display_options['fields']['field_hero_body_content']['table'] = 'field_data_field_hero_body_content';
  $handler->display->display_options['fields']['field_hero_body_content']['field'] = 'field_hero_body_content';
  $handler->display->display_options['fields']['field_hero_body_content']['label'] = '';
  $handler->display->display_options['fields']['field_hero_body_content']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_hero_body_content']['element_default_classes'] = FALSE;
  /* Field: Content: Show CTA */
  $handler->display->display_options['fields']['field_show_cta']['id'] = 'field_show_cta';
  $handler->display->display_options['fields']['field_show_cta']['table'] = 'field_data_field_show_cta';
  $handler->display->display_options['fields']['field_show_cta']['field'] = 'field_show_cta';
  $handler->display->display_options['fields']['field_show_cta']['label'] = '';
  $handler->display->display_options['fields']['field_show_cta']['element_type'] = '0';
  $handler->display->display_options['fields']['field_show_cta']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_show_cta']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_show_cta']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_show_cta']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_show_cta']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_show_cta']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_show_cta']['type'] = 'list_key';
  /* Field: Content: Button Text */
  $handler->display->display_options['fields']['field_button_text']['id'] = 'field_button_text';
  $handler->display->display_options['fields']['field_button_text']['table'] = 'field_data_field_button_text';
  $handler->display->display_options['fields']['field_button_text']['field'] = 'field_button_text';
  $handler->display->display_options['fields']['field_button_text']['label'] = '';
  $handler->display->display_options['fields']['field_button_text']['element_type'] = '0';
  $handler->display->display_options['fields']['field_button_text']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_button_text']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_button_text']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_button_text']['element_default_classes'] = FALSE;
  /* Field: Content: CTA Theme */
  $handler->display->display_options['fields']['field_cta_button_theme']['id'] = 'field_cta_button_theme';
  $handler->display->display_options['fields']['field_cta_button_theme']['table'] = 'field_data_field_cta_button_theme';
  $handler->display->display_options['fields']['field_cta_button_theme']['field'] = 'field_cta_button_theme';
  $handler->display->display_options['fields']['field_cta_button_theme']['label'] = '';
  $handler->display->display_options['fields']['field_cta_button_theme']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_cta_button_theme']['element_type'] = '0';
  $handler->display->display_options['fields']['field_cta_button_theme']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_cta_button_theme']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cta_button_theme']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_cta_button_theme']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_cta_button_theme']['type'] = 'list_key';
  /* Field: Content: CTA Link */
  $handler->display->display_options['fields']['field_cta_hero_link']['id'] = 'field_cta_hero_link';
  $handler->display->display_options['fields']['field_cta_hero_link']['table'] = 'field_data_field_cta_hero_link';
  $handler->display->display_options['fields']['field_cta_hero_link']['field'] = 'field_cta_hero_link';
  $handler->display->display_options['fields']['field_cta_hero_link']['label'] = '';
  $handler->display->display_options['fields']['field_cta_hero_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_cta_hero_link']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_cta_hero_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cta_hero_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_cta_hero_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_cta_hero_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_cta_hero_link']['type'] = 'link_plain';
  /* Field: Content: Hero Content Text Colour */
  $handler->display->display_options['fields']['field_hero_content_text_colour']['id'] = 'field_hero_content_text_colour';
  $handler->display->display_options['fields']['field_hero_content_text_colour']['table'] = 'field_data_field_hero_content_text_colour';
  $handler->display->display_options['fields']['field_hero_content_text_colour']['field'] = 'field_hero_content_text_colour';
  $handler->display->display_options['fields']['field_hero_content_text_colour']['label'] = '';
  $handler->display->display_options['fields']['field_hero_content_text_colour']['element_type'] = '0';
  $handler->display->display_options['fields']['field_hero_content_text_colour']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_hero_content_text_colour']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_hero_content_text_colour']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_hero_content_text_colour']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_hero_content_text_colour']['type'] = 'list_key';
  $handler->display->display_options['fields']['field_hero_content_text_colour']['settings'] = array(
    'data_element_key' => '',
    'skip_safe' => 0,
    'skip_empty_values' => 0,
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_homepage_hero_split' => 'feds_homepage_hero_split',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Promoted to front page status */
  $handler->display->display_options['filters']['promote']['id'] = 'promote';
  $handler->display->display_options['filters']['promote']['table'] = 'node';
  $handler->display->display_options['filters']['promote']['field'] = 'promote';
  $handler->display->display_options['filters']['promote']['value'] = '1';

  /* Display: Feds Homepage Hero Split Block */
  $handler = $view->new_display('block', 'Feds Homepage Hero Split Block', 'feds_homepage_hero_split_block');
  $translatables['feds_homepage_hero_split'] = array(
    t('Master'),
    t('Feds Homepage Hero Split'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Feds Homepage Hero Split Block'),
  );
  $export['feds_homepage_hero_split'] = $view;

  return $export;
}
