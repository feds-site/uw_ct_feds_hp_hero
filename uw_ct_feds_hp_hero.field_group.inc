<?php

/**
 * @file
 * uw_ct_feds_hp_hero.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_hp_hero_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_call_to_action|node|feds_homepage_hero_split|form';
  $field_group->group_name = 'group_call_to_action';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_hero_split';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Call to Action (CTA)',
    'weight' => '13',
    'children' => array(
      0 => 'field_show_cta',
      1 => 'field_button_text',
      2 => 'field_cta_button_theme',
      3 => 'field_cta_hero_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-call-to-action field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_call_to_action|node|feds_homepage_hero_split|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left_side_content|node|feds_homepage_hero_split|form';
  $field_group->group_name = 'group_left_side_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_hero_split';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '12',
    'children' => array(
      0 => 'field_hero_body_content',
      1 => 'field_hero_subtitle',
      2 => 'field_flexible_heading',
      3 => 'field_hero_content_text_colour',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Content',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-left-side-content field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_left_side_content|node|feds_homepage_hero_split|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Call to Action (CTA)');
  t('Content');

  return $field_groups;
}
