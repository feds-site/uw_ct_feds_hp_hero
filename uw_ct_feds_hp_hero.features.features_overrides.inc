<?php

/**
 * @file
 * uw_ct_feds_hp_hero.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_hp_hero_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance
  $overrides["field_instance.node-feds_homepage_hero_split-field_flexible_heading.default_value|0|value"] = '<h2>Waterloo Undergraduate Student Association</h2>
  ';

 return $overrides;
}
