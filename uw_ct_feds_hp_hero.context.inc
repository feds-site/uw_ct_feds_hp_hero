<?php

/**
 * @file
 * uw_ct_feds_hp_hero.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_hp_hero_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_homepage_hero';
  $context->description = 'Large hero image for Feds homepage.';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-a89dab40e58cf9aa89523dcdf701ea87' => array(
          'module' => 'views',
          'delta' => 'a89dab40e58cf9aa89523dcdf701ea87',
          'region' => 'feds_hero',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Large hero image for Feds homepage.');
  $export['feds_homepage_hero'] = $context;

  return $export;
}
